package core;

/**
 * Host, user, pass and port configuration for the Queue connection
 *
 * @author David
 */
public class QueueConfiguration {

    /**
     * The queue host
     */
    private String host = "rabbit";

    /**
     * The queue username
     */
    private String username = "web";

    /**
     * The queue password
     */
    private String password = "services";

    /**
     * The queue port
     */
    private int port = 5672;

    /**
     * Instantiate a new Queue Configuration with the default parameters
     */
    public QueueConfiguration() {
    }

    /**
     * Instantiate a new Queue Configuration with custom parameters
     * @param host The queue host
     * @param username The queue username
     * @param password The queue password
     * @param port The queue port
     */
    public QueueConfiguration(String host, String username, String password, int port) {
        this.host = host;
        this.username = username;
        this.password = password;
        this.port = port;
    }

    /**
     * Get the port
     * @return The queue port
     */
    public int getPort() {
        return port;
    }

    /**
     * Set the queue port
     * @param port The new port
     */
    public void setPort(int port) {
        this.port = port;
    }

    /**
     * Get the queue password
     * @return The password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set a new queue password
     * @param password The new password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Get the queue username
     * @return The username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Set a new queue username
     * @param username The new username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Get the queue host
     * @return The host
     */
    public String getHost() {
        return host;
    }

    /**
     * Set a new queue host
     * @param host The host
     */
    public void setHost(String host) {
        this.host = host;
    }
}
