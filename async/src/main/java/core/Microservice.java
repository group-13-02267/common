package core;

import java.io.Closeable;
import java.io.IOException;

/**
 * Abstract class that all microservice can use to configure them selves before starting
 * @author David
 */
public abstract class Microservice implements Closeable {

    /**
     * The queue connection configuration
     */
    protected final QueueConfiguration queueConfiguration;

    /**
     * Instantiate the microservice with the default queue configuration
     */
    protected Microservice() {
        this.queueConfiguration = new QueueConfiguration();
    }

    /**
     * Configure the startup for the microservice
     * @throws AsyncException Thrown if any asynchronous services on startup fails
     */
    public abstract void startup() throws AsyncException;

    /**
     * Close the microservice connection.
     * @throws IOException Thrown if any of the asynchronous services fails shutting down
     */
    @Override
    public void close() throws IOException {

    }
}
