package core;

/**
 * @param <T> Type of object to send
 * @author David
 */
public interface AsyncSender<T> {
    /**
     * Send a message
     * @param message The message to send
     */
    void send(T message);
}
