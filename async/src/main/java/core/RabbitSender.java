package core;

import com.google.gson.Gson;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

/**
 * core.AsyncSender implementation for the RabbitMQ asynchronous messaging
 *
 * @param <T> The type to send over the RabbitMQ
 * @author David
 */
public class RabbitSender<T> implements AsyncSender<T> {

    /**
     * The queue configuration used to connect to the queue
     */
    private final QueueConfiguration configuration = new QueueConfiguration();

    /**
     * The exchange for receiving the messages
     */
    private final String exchange;

    /**
     * The route to listen on
     */
    private final String route;

    public RabbitSender(String exchange, String route) {
        this.exchange = exchange;
        this.route = route;
    }

    /**
     * Send a message over RabbitMQ
     * @param message The message to send
     */
    @Override
    public void send(T message) {
        try (Connection connection = RabbitHelper.createConnection(configuration);
             Channel channel = connection.createChannel()) {
            channel.exchangeDeclare(exchange, BuiltinExchangeType.TOPIC);

            String body = new Gson().toJson(message);

            System.out.println("Sending on " + route + ": " + message);

            channel.basicPublish(exchange, route, null, body.getBytes(StandardCharsets.UTF_8));
        } catch (TimeoutException | IOException e) {
            e.printStackTrace();
        }
    }
}
