package core;

/**
 * Listener interface for asynchronous communication
 * @param <T> Type of object that it listens for
 * @author David
 */
public interface AsyncListener<T> {
    /**
     * When a message is received
     * @param received The received message
     */
    void receive(T received);

    /**
     * Start listening on the exchange and route
     * @throws AsyncException Thrown if any connection issues rises
     */
    void listen() throws AsyncException;
}
