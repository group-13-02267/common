package core;

/**
 * Async Exception for when asynchronous operations fail
 * @author David
 */
public class AsyncException extends Exception {
    public AsyncException(String message) {
        super(message);
    }

    public AsyncException(String message, Throwable cause) {
        super(message, cause);
    }
}
