package core;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.DeliverCallback;

import java.io.Closeable;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static core.RabbitHelper.createConnection;

/**
 * Abstract class that listeners can inherit from to listen on an asynchronous RabbitMQ exchange and route
 * @param <T> Type of object received from the listener
 * @author David
 */
public abstract class AsyncService<T> implements Closeable, AsyncListener<T> {

    /**
     * The rabbitmq exchange
     */
    private final String exchange;

    /**
     * The queue to listen on
     */
    private final String route;

    /**
     * The rabbitmq connection
     */
    private Connection connection;

    /**
     * Instantiate a new AsyncService to listen on a route and exchange
     * @param exchange The rabbitmq exchange
     * @param route The rabbitmq route to listen on
     */
    public AsyncService(String exchange, String route) {
        this.exchange = exchange;
        this.route = route;
    }

    /**
     * Handle received message and convert to the expected type
     * Must be overridden by the implementor to return the correct type.
     * @param message The message received
     * @return The converted message
     */
    protected abstract T handleMessage(String message);

    /**
     * Start listening on the exchange and route
     * @throws AsyncException Thrown if any connection issues rises
     */
    @Override
    public void listen() throws AsyncException {
        connection = createConnection(new QueueConfiguration());
        Channel channel;

        String queueName;

        try {
            channel = connection.createChannel();
            channel.exchangeDeclare(exchange, BuiltinExchangeType.TOPIC);
            queueName = channel.queueDeclare().getQueue();
            channel.queueBind(queueName, exchange, route);
        } catch (IOException e) {
            throw new AsyncException("Error trying to connection to rabbitmq", e);
        }

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), StandardCharsets.UTF_8);

            String route = delivery.getEnvelope().getRoutingKey();

            T serialized = handleMessage(message);

            if (serialized == null) {
                System.out.println("Error serializing received object: " + message);
                return;
            }

            System.out.println("Received message on " + route + ": " + message);

            receive(serialized);
        };

        try {
            channel.basicConsume(queueName, true, deliverCallback, consumerTag -> System.out.println("Consumer tag?"));
        } catch (IOException e) {
            throw new AsyncException("Error trying to consume queue", e);
        }
    }

    /**
     * Close the connection. Inherited from Closeable.
     * @throws IOException Thrown if the connection fails shutting down
     */
    @Override
    public void close() throws IOException {
        connection.close();
    }
}
