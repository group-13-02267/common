package core;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Helper methods for RabbitMQ
 *
 * @author David
 */
public class RabbitHelper {

    private RabbitHelper() {
    }

    /**
     * Create a connection to the RabbitMQ
     *
     * @param configuration The core.QueueConfiguration for the RabbitMQ
     * @return The created connection
     */
    public static Connection createConnection(QueueConfiguration configuration) {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(configuration.getHost());
        factory.setPort(configuration.getPort());
        factory.setUsername(configuration.getUsername());
        factory.setPassword(configuration.getPassword());
        try {
            return factory.newConnection();
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }
        return null;
    }

}
